#!/usr/bin/python
#
# Author: fccagou
#
#
# macgen.py script to generate a MAC address
# based on https://macaddress.io/faq/how-to-detect-a-virtual-machine-by-its-mac-address
# @see more help at the end of the script
#

import random
from sys import argv, exit


macs = {
    "xen": {
        "description": "Red Hat Xen, XenSource, Novell Xen",
        "range": ["00:16:3E:00:00:00", "00:16:3E:FF:FF:FF"],
        "getmac": lambda: [
            0x00,
            0x16,
            0x3E,
            random.randint(0x00, 0x7F),
            random.randint(0x00, 0xFF),
            random.randint(0x00, 0xFF),
        ],
    },
    "kvm": {
        "description": "KVM (proxmox)",
        "range": ["54:52:00:00:00:00", "54:52:FF:FF:FF:FF"],
        "getmac": lambda: [
            0x54,
            0x52,
            random.randint(0x00, 0xFF),
            random.randint(0x00, 0xFF),
            random.randint(0x00, 0xFF),
            random.randint(0x00, 0xFF),
        ],
    },
    "scvmm": {
        "description": "Microsoft SCVMM (System Center Virtual Machine Manager)",
        "range": ["00:1D:D8:B7:1C:00", "00:1D:D8:F4:1F:FF"],
        "getmac": lambda: [
            0x00,
            0x1D,
            0xD8,
            0xB7,
            random.randint(0x1C, 0x1F),
            random.randint(0x00, 0xFF),
        ],
    },
    "virtualpc": {
        "description": "Microsoft Virtual PC / Virtual Server",
        "range": ["00:03:FF:00:00:00", "00:03:FF:FF:FF:FF"],
        "getmac": lambda: [
            0x00,
            0x03,
            0xFF,
            random.randint(0x00, 0xFF),
            random.randint(0x00, 0xFF),
            random.randint(0x00, 0xFF),
        ],
    },
    "swsoft": {
        "description": "SWsoft",
        "range": ["00:18:51:00:00:00", "00:18:51:FF:FF:FF"],
        "getmac": lambda: [
            0x00,
            0x18,
            0x51,
            random.randint(0x00, 0xFF),
            random.randint(0x00, 0xFF),
            random.randint(0x00, 0xFF),
        ],
    },
    "bhyve": {
        "description": "bhyve by FreebsdF",
        "range": ["58:9C:FC:00:00:00", "58:9C:FC:FF:FF:FF"],
        "getmac": lambda: [
            0x58,
            0x9C,
            0xFC,
            random.randint(0x00, 0xFF),
            random.randint(0x00, 0xFF),
            random.randint(0x00, 0xFF),
        ],
    },
    "nutanix": {
        "description": "Nutanix AHV",
        "range": ["50:6B:8D:00:00:00", "50:6B:8D:FF:FF:FF"],
        "getmac": lambda: [
            0x50,
            0x68,
            0x8D,
            random.randint(0x00, 0xFF),
            random.randint(0x00, 0xFF),
            random.randint(0x00, 0xFF),
        ],
    },
    "hetzner": {
        "description": "Hetzner vServer (based on KVM and libvirt)  Self tested",
        "range": ["96:00:00:00:00:00", "96:00:FF:FF:FF:FF"],
        "getmac": lambda: [
            0x96,
            random.randint(0x00, 0xFF),
            random.randint(0x00, 0xFF),
            random.randint(0x00, 0xFF),
            random.randint(0x00, 0xFF),
            random.randint(0x00, 0xFF),
        ],
    },
}

if len(argv) == 1:
    argv.append("help")

if argv[1] not in macs:
    print(f"Usage: {argv[0]} <hypervisor type>")
    print("\n")
    print("HYPERVISOR TYPES")
    for m in macs:
        print(f"  {m:<10}: {macs[m]['range']} {macs[m]['description']}")
    print("\n")

    if argv[1] in ("help", "-h", "--help"):
        exit(0)

    exit(1)

print(":".join(map(lambda x: "%02x" % x, macs[argv[1]]["getmac"]())))


#
# DOCUMENTATION
#
# Red Hat Xen, XenSource, Novell Xen  00:16:3E:00:00:00 to 00:16:3E:FF:FF:FF
#
# https://wiki.xenproject.org/wiki/Xen_Networking
# https://mcpmag.com/articles/2007/11/27/hey-vm-whats-your-hypervisor.aspx
# https://www.techrepublic.com/blog/data-center/mac-address-scorecard-for-common-virtual-machine-platforms
#
# Microsoft SCVMM (System Center Virtual Machine Manager)  00:1D:D8:B7:1C:00 to 00:1D:D8:F4:1F:FF
#
# http://techgenix.com/mac-address-pool-duplication-hyper-v/
# https://docs.microsoft.com/en-us/system-center/vmm/network-mac?view=sc-vmm-1807
# https://blogs.technet.microsoft.com/gbanin/2014/08/27/how-to-solve-mac-address-conflict-on-hyper-v/
#
# Microsoft Virtual PC / Virtual Server  00:03:FF:00:00:00 to 00:03:FF:FF:FF:FF
#
# https://mcpmag.com/articles/2007/11/27/hey-vm-whats-your-hypervisor.aspx
# https://www.techrepublic.com/blog/data-center/mac-address-scorecard-for-common-virtual-machine-platforms/
# https://blogs.technet.microsoft.com/medv/2011/01/24/how-to-manage-vm-mac-addresses-with-the-globalimagedata-xml-file-in-med-v-v1/
#
# SWsoft  00:18:51:00:00:00 to 00:18:51:FF:FF:FF
#
# https://mcpmag.com/articles/2007/11/27/hey-vm-whats-your-hypervisor.aspx
#
# bhyve by FreebsdF  58:9C:FC:00:00:00 to 58:9C:FC:FF:FF:FF
#
# https://macaddress.io/statistics/company/17619
#
# Nutanix AHV  50:6B:8D:00:00:00 to 50:6B:8D:FF:FF:FF
#
# https://macaddress.io/statistics/company/17388
#
# KVM (proxmox)  54:52:00:00:00:00 to 54:52:FF:FF:FF:FF
#
# https://www.centos.org/forums/viewtopic.php?t=26739
#
# Hetzner vServer (based on KVM and libvirt)  96:00:00:00:00:00 to 96:00:FF:FF:FF:FF  Self tested

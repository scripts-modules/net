# ----------------------------------
# network
# ----------------------------------

is_link_exist () {
    ip link show "$1" >/dev/null 2>&1
}

get_link_macaddr () {
    local linkname="$1"
    ip link show  "${linkname}"| grep 'link/ether' | awk '{ print $2 }'
}

net_macaddrgen () {
    local hypervisor_type
    hypervisor_type="$1"
    python "${BASH_SOURCE[0]%/*}"/files/macaddrgen.py "${hypervisor_type}"
}


# ----------------------------------
# object's format
# ----------------------------------
check_hostname () {
    local h_name="$1"
    info "checking ${h_name}"
    warning "${FUNCNAME[0]}: Not implemented"
    [ -z "${h_name}" ] && exit_on_error "Hostname is empty"
}

check_macaddr () {
    local h_macaddr="$1"
    info "checking ${h_macaddr}"
    warning "${FUNCNAME[0]}: Not implemented"
    [ -z "${h_macaddr}" ] && exit_on_error "macaddr is empty"
}

check_ip () {
    local h_ip="$1"
    info "checking ${h_ip}"
    warning "${FUNCNAME[0]}: Not implemented"
    [ -z "${h_ip}" ] && exit_on_error "ip is empty"
}

check_link () {
    local h_link="$1"
    info "checking ${h_ip}"
    warning "${FUNCNAME[0]}: Not implemented"
    [ -z "${h_link}" ] && exit_on_error "link is empty"
}
